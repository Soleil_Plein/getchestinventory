﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class XUiC_GetChestInventory : XUiController
{
    private List<ItemStack> chestItems = new List<ItemStack>();
    private XUiC_CreativeStackGrid creativeStackGrid;
    private XUiV_Label ownerName;
    private XUiV_Label chestName;
    private string apiSteamKey = "483EE97EAD599BA40E69D4E7CADD3973";
    private bool flag = true;

    public override void Init()
    {
        base.Init();
        creativeStackGrid = Parent.GetChildByType<XUiC_CreativeStackGrid>().GetChildById("inventory") as XUiC_CreativeStackGrid;
        XUiController controller1 = GetChildById("ownerName");
        ownerName = controller1.ViewComponent as XUiV_Label;
        XUiController controller2 = GetChildById("chestName");
        chestName = controller2.ViewComponent as XUiV_Label;
    }

    public override void OnOpen()
    {
        base.OnOpen();
        creativeStackGrid.SetSlots(chestItems.ToArray());
        ownerName.Text = string.Empty;
        chestName.Text = string.Empty;
    }

    private void LookAtSecureChest(Vector3i blockPos)
    {
        try
        {
            if (GetTilesEntities(blockPos))
                creativeStackGrid.SetSlots(GetItemStacksFromTileEntity(blockPos));
        }
        catch (Exception ex)
        {
            Log.Out("LookAtSecureChest " + ex.Message);
        }
    }
    
    public override void Update(float _dt)
    {
        try 
        {
            base.Update(_dt);
            string holdingItemName = xui.playerUI.entityPlayer.inventory.holdingItem.Name;

            if (holdingItemName == "gunToolDiggerAdmin" || holdingItemName == "meleeToolSalvageWrenchAdmin" || holdingItemName == "gunHandgunPistolAdmin" || holdingItemName == "meleeToolHammerOfGodAdmin" )
            {
                if (Input.GetMouseButtonUp(2))
                {
                    viewComponent.IsVisible = true;
                    RayPosition();
                }
            }
            if (!(holdingItemName == "gunToolDiggerAdmin" || holdingItemName == "meleeToolSalvageWrenchAdmin" || holdingItemName == "gunHandgunPistolAdmin" || holdingItemName == "meleeToolHammerOfGodAdmin"))
            {
                viewComponent.IsVisible = false;
                creativeStackGrid.SetSlots(chestItems.ToArray());
                ownerName.Text = string.Empty;
                chestName.Text = string.Empty;
                return;
            }
        }
        catch (Exception ex)
        {
            Log.Out("Update " + ex.Message);
        }
    }

    public void RayPosition()
    {
        flag = true;
        Vector3i focusBlockPos = xui.playerUI.entityPlayer.HitInfo.hit.blockPos;
        BlockValue blockValue = GameManager.Instance.World.GetBlock(xui.playerUI.entityPlayer.HitInfo.hit.clrIdx, focusBlockPos);
        if (blockValue.ischild)
        {
            focusBlockPos = Block.list[blockValue.type].multiBlockPos.GetParentPos(focusBlockPos, blockValue);
            blockValue = GameManager.Instance.World.GetBlock(xui.playerUI.entityPlayer.HitInfo.hit.clrIdx, focusBlockPos);
        }
        if (!blockValue.Equals(BlockValue.Air) && Block.list[blockValue.type].GetBlockActivationCommands(GameManager.Instance.World, blockValue, xui.playerUI.entityPlayer.HitInfo.hit.clrIdx, focusBlockPos, xui.playerUI.entityPlayer).Length != 0)
        {
            creativeStackGrid.SetSlots(GetItemStacksFromTileEntity(focusBlockPos));
        }
    }
    
    private bool GetTilesEntities(Vector3i blockPos)
    {
        try
        {
            Chunk chunk = (Chunk)GameManager.Instance.World.GetChunkFromWorldPos(blockPos);
            int tileNumber = 0;
            foreach (TileEntity tileEntity1 in chunk.GetTileEntities().list)
            {
                if (tileEntity1.ToWorldPos() == blockPos)
                    return true;
                tileNumber += 1;
                Log.Out($"TileEntity n°{tileNumber} / Nom : {tileEntity1.blockValue.Block.GetBlockName()} / Position : {tileEntity1.ToWorldPos()}");
            }
            return false;
        }
        catch (Exception ex)
        {
            Log.Out("GetTilesEntities " + ex.Message);
        }
        return false;
    }

    private ItemStack[] GetItemStacksFromTileEntity(Vector3i blockPos)
    {
        try
        {
            TileEntityLootContainer tileEntity = (TileEntityLootContainer)GameManager.Instance.World.GetTileEntity(0, blockPos);
            ownerName.Text = string.Empty;
            if (tileEntity is TileEntitySecureLootContainer && flag)
            {
                GetChestOwnerSecureLoot(blockPos);
                flag = false;
            }
            chestName.Text = tileEntity.blockValue.Block.GetLocalizedBlockName().ToUpper();
            return tileEntity.GetItems();
        }
        catch (Exception ex)
        {
            Log.Out("GetItemStacksFromTileEntity " + ex.Message);
        }
        return null;
    }

    private void GetChestOwnerSecureLoot(Vector3i blockPos)
    {
        try
        {
            TileEntitySecureLootContainer tileEntity = (TileEntitySecureLootContainer)GameManager.Instance.World.GetTileEntity(0, blockPos);
            WebClient webClient = new WebClient();
            webClient.DownloadStringAsync(new Uri("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/?key=" + apiSteamKey + "&format=json&steamids=" + tileEntity.GetOwner()));
            if (tileEntity.GetOwner().PlatformIdentifierString.Length != 0)
            {
                ownerName.Text = $"EOS ID du propriétaire : {tileEntity.GetOwner()}";
            }
        }
        catch (Exception ex)
        {
            Log.Out("GetChestOwnerSecureLoot " + ex.Message);
        }
    }

    private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
    {
        JsonData jsonData = new JsonData();
        jsonData = JsonMapper.ToObject(e.Result);
        Log.Out($"{jsonData[0][0][0][3]}");
        if (!ownerName.Text.Contains(jsonData[0][0][0][3].ToString()))
            ownerName.Text += $" / Nom du joueur : {jsonData[0][0][0][3]}";
    }
}